# Dotfiles

My config files for 

- [alacritty](https://github.com/alacritty/alacritty)
- [tmux](https://github.com/tmux/tmux)
- [zsh](https://github.com/zsh-users/zsh)
- [neovim](https://neovim.io/)
